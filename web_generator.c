#include <stdio.h>

char* web_template = "<!DOCTYPE html>\n\
<html>\n\
    <head>\n\
        <title>C-generated web page</title>\n\
    </head>\n\
    <body>\n\
        <p>This is an example of a simple HTML page generated with a C program.</p>\n\
    </body>\n\
</html>\n\
";

int main() {
  printf("%s", web_template);
  
  return 0;
}
